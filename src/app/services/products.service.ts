import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from '../models/product';
import {GLOBAL} from './global';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  public url: string;

  constructor(
    //injectar sericio http para realizar peticiones
    public _http: HttpClient
  ){
    this.url = GLOBAL.url;
  }

  getProducts(): Observable<any>{
    let headers = new HttpHeaders()
      .set("Content-Type", "application/x-www-form-urlencoded");

    return this._http.get(this.url + 'productos', {headers: headers});
  }

  //TODO crear servicio del carrito 
  getTotal(productos: string[], idTarjeta: string): Observable<any>{
    let headers = new HttpHeaders()
      .set("Content-Type", "application/x-www-form-urlencoded");

    return this._http.post(this.url + 'total', {
      'productos' : JSON.stringify(productos),
      'tarjetaId' : idTarjeta
      }
    );
  }

  realizarCompra(idClient: string, productos: string[], idTarjeta: string): Observable<any> {
    let headers = new HttpHeaders()
      .set("Content-Type", "application/x-www-form-urlencoded");

    return this._http.post(this.url + 'realizarCompra', {
      'productos' : JSON.stringify(productos),
      'idTarjeta' : idTarjeta,
      'idClient' : idClient
      }
    );
  } 

  getProduct(): Observable<any>{
    let headers = new HttpHeaders()
      .set("Content-Type", "application/x-www-form-urlencoded");

    return this._http.get(this.url + 'dameProducto/', {headers: headers});
  }

  editProduct(product: any): Observable<any> {
    let headers = new HttpHeaders()
      .set("Content-Type", "application/x-www-form-urlencoded");

    return this._http.post(this.url + 'actualizarProducto', product);
  }   
}
