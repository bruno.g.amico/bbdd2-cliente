import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {GLOBAL} from './global';

@Injectable({
  providedIn: 'root'
})
export class PromotionsService {

  public url: string;

  constructor(
    public _http: HttpClient
  ){
    this.url = GLOBAL.url;
  }

  getPromotionsPago(): Observable<any>{
    let headers = new HttpHeaders()
      .set("Content-Type", "application/x-www-form-urlencoded");

    return this._http.get(this.url + 'promopagos', {headers: headers});
  } 

  getPromotionsMarca(): Observable<any>{
    let headers = new HttpHeaders()
      .set("Content-Type", "application/x-www-form-urlencoded");

    return this._http.get(this.url + 'promomarcas', {headers: headers});
  }    
}
