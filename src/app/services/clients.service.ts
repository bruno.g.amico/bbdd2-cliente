import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {GLOBAL} from './global';

@Injectable({
  providedIn: 'root'
})
export class ClientsService {

  public url: string;

  constructor(
    public _http: HttpClient
  ){
    this.url = GLOBAL.url;
  }

  getClient(): Observable<any>{
    let headers = new HttpHeaders()
      .set("Content-Type", "application/x-www-form-urlencoded");

    return this._http.get(this.url + 'dameCliente/', {headers: headers});
  }  

  getTarjetsClient(idClient: string): Observable<any>{
    let headers = new HttpHeaders()
      .set("Content-Type", "application/x-www-form-urlencoded");

    return this._http.get(this.url + 'tarjetas/' + idClient, {headers: headers});
  }

  getVentas(idClient: string): Observable<any>{
    let headers = new HttpHeaders()
      .set("Content-Type", "application/x-www-form-urlencoded");

    return this._http.get(this.url + 'ultimasVentas/' + idClient, {headers: headers});
  }      
}
