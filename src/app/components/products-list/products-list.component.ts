import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute, Params } from '@angular/router';
import { Product } from '../../models/product';
import { ProductsService } from '../../services/products.service';
import { PromotionsService } from '../../services/promotions.service';
import { ClientsService } from '../../services/clients.service';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css'],
  providers: [ProductsService, PromotionsService, ClientsService]
})
export class ProductsListComponent implements OnInit {

  public title: string;
  public products: any;
  public promotionsPago: any;
  public promotionsMarca: any;
  public client: any;
  public idClient: string;
  public messageCompra: string;
  public tarjets: any;
  public seleccionado: string;
  public tarjetaSeleccionada: string;
  public seleccionados: string[];
  public totalPrice: number;
  public errorTotalProductos: string;
  public ultimasVentas: any;
  public ultimasVentasJson: any;
  public errorVentas: string;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _productsService: ProductsService, //cargo el servicio
    private _promotionsService: PromotionsService, //cargo el servicio
    private _clientsService: ClientsService, //cargo el servicio
  ) {
    this.title = 'Inicio';
    this.seleccionado = '';
    this.tarjetaSeleccionada = '';
    this.seleccionados=[];
    this.totalPrice = 0;
    this.errorTotalProductos = '';
    this.client = {'nombres':'','apellido':''};
    
    //this.idClient = '1'; //enrique molinari objectdb
    this.idClient = '7'; //enrique molinari en MYSQL
    this.messageCompra = '';
    this.errorVentas = '';
  }

  ngOnInit(){
    this.getClient();    
    this.getProducts();
    this.getPromotionsMarca();
    this.getPromotionsPago();
    //objectdb
    //this.getTarjetsClient('1');
    //hibernate
    this.getTarjetsClient('7');
    this.getUltimasVentasClient('7');
    this.errorTotalProductos = '';
    this.messageCompra = '';
  }

  getProducts(){
    this._productsService.getProducts().subscribe(
      response => {        
        this.products = response.productos;
        if(response.result == 'success'){
          this.products = response.productos;
        }
      },
      error => {
        console.log(error);
      }
    )
  }

  getPromotionsPago(){
    this._promotionsService.getPromotionsPago().subscribe(
      response => {
        this.promotionsPago = response;
        if(response.status == 'success'){
          this.promotionsPago = response;
        }
      },
      error => {
        console.log(error);
      }
    )
  }

  getPromotionsMarca(){
    this._promotionsService.getPromotionsMarca().subscribe(
      response => {
        this.promotionsMarca = response;
        if(response.status == 'success'){
          this.promotionsMarca = response;
        }
      },
      error => {
        console.log(error);
      }
    )
  }    

  getClient(){
    this._clientsService.getClient().subscribe(
      response => {
        if(response.result == 'success'){
          this.client = response.cliente;
        }
      },
      error => {
        console.log(error);
      }
    )
  }   

  getTarjetsClient(idClient: string){
    this._clientsService.getTarjetsClient(idClient).subscribe(
      response => {
        console.log(response);
        if(response.result == 'success'){
          this.tarjets = response.tarjetas;
        }
      },
      error => {
        console.log(error);
      }
    )
  }

  getUltimasVentasClient(idClient: string){
    this._clientsService.getVentas(idClient).subscribe(
      response => {
        console.log('Ultimas ventas: ', response, response.result);
        if(response.result == 'success'){
          //this.ultimasVentas = response.ventas;
          this.ultimasVentas = JSON.stringify(response.ventas, null, 2);
          this.ultimasVentasJson = response.ventas.map((ultimasVentas: string) => JSON.parse(ultimasVentas));
        }
      },
      error => {
        console.log(error);
      }
    )
  }  

  showTotalPrice() { 
    this._productsService.getTotal(this.seleccionados, this.tarjetaSeleccionada).subscribe(
      response => {
        this.errorTotalProductos = '';
        if(response.result == 'success'){
          this.totalPrice = response.total;
        }else if(response.result == 'error'){
          this.errorTotalProductos = response.message;
        }
      },
      error => {
        console.log(error);
      }
    )
  } 

  buy() { 
    this._productsService.realizarCompra(this.idClient, this.seleccionados, this.tarjetaSeleccionada).subscribe(
      response => {
        this.messageCompra = '';
        if(response.result == 'success'){
          this.messageCompra = 'Compra realizada con exito!';
          this.getUltimasVentasClient(this.idClient);
        }else if(response.result == 'error'){
          this.messageCompra = response.message;
        }
      },
      error => {
        console.log(error);
      }
    )
  }    
}
