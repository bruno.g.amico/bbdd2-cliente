import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute, Params } from '@angular/router';
import { Product } from '../../models/product';
import { ProductsService } from '../../services/products.service';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css'],
  providers: [ProductsService]
})
export class EditProductComponent implements OnInit {
  
  formProduct: FormGroup;
  public product: any;
  public idProduct: string;
  public message: string;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _productsService: ProductsService, //cargo el servicio
    public formulario: FormBuilder
  ) { 
    this.idProduct = '1';
    this.message = '';
    this.formProduct = this.formulario.group({
      descripcion : [''],
      marca : [''],
      precio : [''],
      codigo : [''],
      id : [''],
      version : [''],
    });
  }

  ngOnInit(): void {
    this.getProduct();
    this.message = ''; 
  }

  sendForm(): any {
    console.log('me presionaste');
    console.log(this.formProduct.value);
    //el id esta hardcode
    this._productsService.editProduct(this.formProduct.value).subscribe(
      response => {
        //this.messageCompra = '';
        if(response.result == 'success'){
          this.message = 'Actualización ok!';
        }else if(response.result == 'error'){
          this.message = response.message;
        }
      },
      error => {
        console.log(error);
      }
    )
  }      

  getProduct(){
    this._productsService.getProduct().subscribe(
      response => {
        console.log(response);
        if(response.result == 'success'){
          this.product = response.producto;
          this.formProduct.setValue({
            id:response.producto.id,
            descripcion:response.producto.descripcion,
            codigo:response.producto.codigo,
            precio:response.producto.precio,
            marca:response.producto.marca.descripcion,
            version:response.producto.version,            
          });
        }
      },
      error => {
        console.log(error);
      }
    )
  }     
}
