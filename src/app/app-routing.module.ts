import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

//components
import { ProductsListComponent } from './components/products-list/products-list.component';
import { EditProductComponent } from './components/edit-product/edit-product.component';

const routes: Routes = [
  {path: '', pathMatch:'full', redirectTo:'ProductsListComponent'},
  {path: 'products-list', component: ProductsListComponent},
  {path: 'edit-product', component: EditProductComponent  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
